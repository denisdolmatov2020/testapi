from room.views import RoomsListAPIView
from django.urls import path


urlpatterns = [
    path('', RoomsListAPIView.as_view(), name='rooms-list')
]
