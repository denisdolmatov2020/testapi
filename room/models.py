from django.db import models
import uuid as uuid


class Room(models.Model):
    uuid = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    title = models.CharField(verbose_name='Название зала', max_length=48)
    color = models.CharField(verbose_name='Цвет зала', default='#FFFFFF', max_length=7)

    def __str__(self):
        return '{} {} {}'.format(self.uuid, self.title, self.color)
