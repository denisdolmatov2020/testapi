from rest_framework.generics import ListAPIView
from room.models import Room
from room.serializers import RoomSerializer
from testApi.filters import MainFilter
from testApi.services import CustomPagination


class RoomsListAPIView(ListAPIView):
    serializer_class = RoomSerializer
    pagination_class = CustomPagination
    filter_class = MainFilter
    queryset = Room.objects.all()
