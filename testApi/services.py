import uuid
from rest_framework.pagination import PageNumberPagination


class CustomPagination(PageNumberPagination):
    page_size = 3
    page_size_query_param = 'count'


def parse_array(ids_string):
    return [uuid.UUID(x.strip()).hex for x in ids_string.split(',') if x]