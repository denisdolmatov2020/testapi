from django_filters.rest_framework import FilterSet
from testApi.services import parse_array


class MainFilter(FilterSet):
    def filter_queryset(self, queryset):
        ids = self.request.query_params.get('id', None)
        if ids is not None:
            queryset = queryset.filter(uuid__in=parse_array(ids))
        exclude_ids = self.request.query_params.get('excludeId', None)
        if exclude_ids is not None:
            queryset = queryset.exclude(uuid__in=parse_array(exclude_ids))
        return queryset
