import uuid as uuid
from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """ User model """
    uuid = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)

    def __str__(self):
        return "{} {}".format(self.uuid, self.username)
