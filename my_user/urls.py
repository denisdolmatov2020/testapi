from my_user.views import LoginAPIView, LogoutAPIView
from django.urls import path


urlpatterns = [
    path('login/', LoginAPIView.as_view(), name='login'),
    path('logout/', LogoutAPIView.as_view(), name='logout')
]
