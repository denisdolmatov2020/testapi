from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from meet.models import Meet
from meet.serializers import MeetSerializer
from testApi.filters import MainFilter
from testApi.services import CustomPagination
from rest_framework.response import Response
from rest_framework.decorators import api_view


class MeetsListAPIView(ListAPIView):
    serializer_class = MeetSerializer
    pagination_class = CustomPagination
    queryset = Meet.objects.all().order_by('uuid')
    filter_class = MainFilter


@api_view(['POST', 'DELETE'])
def users_meet(request, uuid):
    try:
        meet = Meet.objects.get(uuid=uuid)
        if request.method == 'POST':
            meet.users.add(request.user)
        elif request.method == 'DELETE':
            meet.users.remove(request.user)
        meet.save()
        return Response(status=status.HTTP_200_OK)
    except:
        return Response(status=status.HTTP_400_BAD_REQUEST)
