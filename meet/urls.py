from meet.views import MeetsListAPIView, users_meet
from django.urls import path


urlpatterns = [
    path('', MeetsListAPIView.as_view(), name='meets-list'),
    path('<uuid:uuid>/member/', users_meet)
]
