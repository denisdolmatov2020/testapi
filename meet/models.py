from django.db import models
import uuid as uuid
from room.models import Room
from testApi.settings import AUTH_USER_MODEL


class Meet(models.Model):
    uuid = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    time = models.DateTimeField(verbose_name='Время встречи')
    title = models.CharField(verbose_name='Название встречи', max_length=48)
    users = models.ManyToManyField(AUTH_USER_MODEL, verbose_name='Участники встречи', related_name='meet_up_users')
    room = models.ForeignKey(Room, verbose_name='Зал для встречи', on_delete=models.CASCADE)

    def __str__(self):
        return '{} {} {}'.format(self.time, self.title, self.time)
